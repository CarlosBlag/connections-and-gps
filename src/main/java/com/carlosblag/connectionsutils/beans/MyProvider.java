package com.carlosblag.connectionsutils.beans;

/**
 * Created by cblanco on 10/10/14.
 */
public class MyProvider {

    String provider;
    boolean isEnable;

    public MyProvider(String provider, boolean isEnable) {
        this.provider = provider;
        this.isEnable = isEnable;
    }

    public String getProviderName() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public boolean isEnable() {
        return isEnable;
    }

    public void setEnable(boolean isEnable) {
        this.isEnable = isEnable;
    }
}
