package com.carlosblag.connectionsutils;

import android.content.Context;
import android.location.LocationManager;

/**
 * Created by cblanco on 10/10/14.
 */
public class GPSStatusUtils {

    public static boolean isGpsEnable(Context context){


        LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE );
        return  manager.isProviderEnabled(LocationManager.GPS_PROVIDER);



    }

    public static boolean isNetworkEnable(Context context){


        LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE );
        return  manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);



    }


}
