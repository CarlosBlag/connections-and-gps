package com.carlosblag.connectionsutils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by cblanco on 10/10/14.
 */
public class ConnectionUtils {

    public static int isOnline(Context context) {


        try {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo info = cm.getActiveNetworkInfo();
            if (info.isConnected()) {

                if (info.getType() == cm.TYPE_WIFI) {
                    return cm.TYPE_WIFI;
                } else if (info.getType() == cm.TYPE_MOBILE) {
                    return cm.TYPE_MOBILE;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }


    public static String checkActiveNetworkState(Context context) {
        String result = "";
        ConnectivityManager mConnectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (mConnectivityManager != null) {
            NetworkInfo mNetworkInfo = mConnectivityManager.getActiveNetworkInfo();
            if (mNetworkInfo != null) {
                result = mNetworkInfo.getSubtypeName();
            }
        }
        return result;
    }
}
