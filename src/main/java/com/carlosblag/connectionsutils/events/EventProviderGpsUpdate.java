package com.carlosblag.connectionsutils.events;


import com.carlosblag.connectionsutils.beans.MyProvider;

/**
 * Created by cblanco on 10/10/14.
 */
public class EventProviderGpsUpdate {

    String providerName;
    MyProvider provider;


    public EventProviderGpsUpdate(String providerName, boolean isEnable) {
        this.providerName = providerName;
        provider = new MyProvider(providerName,isEnable);
    }

    public String getProviderName() {
        return providerName;
    }

    public MyProvider getMyProvider(){
        return provider;
    }
}
