package com.carlosblag.connectionsutils.events;

import android.location.Location;

/**
 * Created by cblanco on 10/10/14.
 */
public class EventUpdateLocation {

    Location location;

    public EventUpdateLocation(Location location){
        this.location=location;
    }

    public EventUpdateLocation() {

    }

    public Location getLocation() {
        return location;
    }

    public double getLatitude(){
        return location.getLatitude();
    }

    public double getLongitude(){
        return location.getLongitude();


    }

    public double getAccuracy(){
        return location.getAccuracy();
    }

}
